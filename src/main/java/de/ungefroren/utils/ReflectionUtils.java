package de.ungefroren.utils;

import com.google.common.base.Joiner;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 27.09.2017.
 */
public class ReflectionUtils {

    public static Field getField(Class<?> clazz, String name) {
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Can't find field " + name, e);
        }
    }

    public static Method getMethod(Class<?> clazz, String name, Class<?>... param) {
        try {
            Method method = clazz.getDeclaredMethod(name, param);
            method.setAccessible(true);
            return method;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Can't find method " + name + " for parameter types " + Joiner.on(", ").join(param), e);
        }
    }

    public static Object getField(Object o, String name) throws IllegalAccessException {
        return ReflectionUtils.getField(o.getClass(), name).get(o);
    }

    public static Object invokeMethod(Object o , String name, Object... param) throws IllegalAccessException {
        Class<?>[] classes = new Class[param.length];
        for (int i = 0; i < param.length; i++) {
            classes[i] = param[i].getClass();
        }
        try {
            return ReflectionUtils.getMethod(o.getClass(), name, classes).invoke(o, param);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Can't invoke method " + name + " for parameters " + Joiner.on(", ").join(param), e);
        }
    }
}
