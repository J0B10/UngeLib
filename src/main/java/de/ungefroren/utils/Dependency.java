package de.ungefroren.utils;


import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jonas on 10.09.2017.
 */
public abstract class Dependency {

    private JavaPlugin mainPlugin;
    private boolean isHooked = false;
    private JavaPlugin instance;

    public Dependency(JavaPlugin plugin) {
        this.mainPlugin = plugin;
    }

    public final void hook(String pluginName) {
        try {
            instance = loadInstance();
            if (checkVersion()) {
                mainPlugin.getLogger().log(Level.INFO, mainPlugin.getName() + " hooked into " + instance.getName() + " !");
                isHooked = true;
            } else {
                mainPlugin.getLogger().log(Level.WARNING, mainPlugin.getName() + " could not hook into " + pluginName + ":\n" +
                        "Incompatible Version of " + instance.getName() + " installed!");
                isHooked = false;
            }
        } catch (NoClassDefFoundError e) {
            mainPlugin.getLogger().log(Level.WARNING, mainPlugin.getName() + " could not hook into " + pluginName + ":\n" +
                    pluginName + " not found!");
            isHooked = false;
        }
    }

    public final boolean isHooked() {
        return isHooked;
    }

    public final JavaPlugin inst() {
        return instance;
    }

    public abstract boolean checkVersion();

    protected final boolean defaultVersionCheck(String minimumRequiredVersion) {
        try {
            String currentVersion = inst().getDescription().getVersion();
            Pattern regex = Pattern.compile("[0-9]+.[0-9]+(.[0-9]+)?");
            Matcher mCurrent = regex.matcher(currentVersion);
            Matcher mRequired =  regex.matcher(minimumRequiredVersion);
            currentVersion = mCurrent.group();
            minimumRequiredVersion = mRequired.group();
            String[] minimumVersionNumbers = minimumRequiredVersion.split("\\.");
            String[] currentVersionNumbers = currentVersion.split("\\.");
            for (int i = 0; i < currentVersionNumbers.length && i < minimumVersionNumbers.length; i++) {
                int current = Integer.parseInt(currentVersionNumbers[i]);
                int minimum = Integer.parseInt(minimumVersionNumbers[i]);
                if (current > minimum) return true;
                else if (current < minimum) return false;
            }
            if (currentVersionNumbers.length <= minimumVersionNumbers.length) return true;
            else return false;
        } catch (IllegalStateException | NumberFormatException e) {
            throw new RuntimeException("Couldn't check " + inst().getName() + " version!", e);
        }
    }

    protected abstract JavaPlugin loadInstance() throws NoClassDefFoundError;
}
