package de.ungefroren.utils.config;

import de.ungefroren.utils.SimpleConfigAbstract;
import org.bukkit.configuration.InvalidConfigurationException;

import java.io.File;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 09.10.2017.
 */
public class UngeLibConfig extends SimpleConfigAbstract {

    public final boolean warpCompleterEnabled;
    public final boolean WGRegionCompleterEnabled;
    public final boolean buildUtilsEnabled;
    public final boolean pluginManagerEnabled;

    public UngeLibConfig(File file) throws InvalidConfigurationException {
        super(file);
        warpCompleterEnabled = new DefaultOption<Boolean>(false) {
            @Override
            protected Boolean of() throws Missing, Invalid {
                return getBoolean("modules.WarpCompleter");
            }
        } .get();
        WGRegionCompleterEnabled = new DefaultOption<Boolean>(false) {
            @Override
            protected Boolean of() throws Missing, Invalid {
                return getBoolean("modules.WGRegionCompleter");
            }
        } .get();
        buildUtilsEnabled = new DefaultOption<Boolean>( false) {
            @Override
            protected Boolean of() throws Missing, Invalid {
                return getBoolean("modules.BuildUtils");
            }
        }.get();
        pluginManagerEnabled = new DefaultOption<Boolean>(false) {
            @Override
            protected Boolean of() throws Missing, Invalid {
                return getBoolean("modules.PluginManager");
            }
        }.get();
    }
}
