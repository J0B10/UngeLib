package de.ungefroren.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 18.09.2017.
 */
public class Math {

    public static double calculate(String calculation) throws IllegalArgumentException {
        calculation = calculation.replace(" ", "");
        while (true) {
            Matcher minus = Pattern.compile("[0-9\\])]-[0-9(\\[]").matcher(calculation);
            if (!minus.find()) break;
            calculation = calculation.substring(0, minus.start() + 1) + "~" + calculation.substring(minus.end() - 1);
        }
        return calc(calculation);
    }

    private static double calc(String calculation)throws IllegalArgumentException {
        final String douBle = "-?[0-9]+(\\.[0-9]+)?";
        final String operation = "[~+*/^]";
        final String brackets = "[\\[\\](|)]";
        if (!calculation.matches("(" + operation + "?" + brackets + "*" + douBle + brackets + "*)+"))
            throw new IllegalArgumentException("Invalid calculation format");
        //Resolve braces
        while (true) {
            Matcher mBrackets = Pattern.compile("[\\[|(][^|()\\[\\]]*[]|)]").matcher(calculation);
            if (!mBrackets.find()) break;
            String group = mBrackets.group();
            double result = calc(group.substring(1, group.length() - 1));
            if (group.startsWith("|")) result = java.lang.Math.abs(result);
            System.out.println(group + "=" + result);       //DEBUG
            calculation = calculation.substring(0, mBrackets.start()) + result + calculation.substring(mBrackets.end());
        }
        Matcher temp = Pattern.compile("[\\[\\])|(]").matcher(calculation);
        if (temp.find()) throw new IllegalArgumentException("Unbalanced bracket at column " + (temp.start() + 0));

        // Potency
        while (true) {
            Matcher pow = Pattern.compile(douBle + "\\^" + douBle).matcher(calculation);
            if (!pow.find()) break;
            String group = pow.group();
            int i = group.indexOf("^");
            double result = java.lang.Math.pow(Double.parseDouble(group.substring(0, i)),
                                               Double.parseDouble(group.substring(i + 1)));
            System.out.println(group + "=" + result);       //DEBUG
            calculation = calculation.substring(0, pow.start()) + result + calculation.substring(pow.end());
        }
        temp = Pattern.compile("\\^").matcher(calculation);
        if (temp.find()) throw new IllegalArgumentException("Invalid operation at " + (temp.start() + 1));


        // Multiply or divide
        while (true) {
            Matcher mOd = Pattern.compile(douBle + "[*/]" + douBle).matcher(calculation);
            if (!mOd.find()) break;
            String group = mOd.group();
            int m = group.indexOf("*");
            int d = group.indexOf("/");
            double result;
            if (m != -1) result = Double.parseDouble(group.substring(0, m)) * Double.parseDouble(group.substring(m + 1));
            else result = Double.parseDouble(group.substring(0, d)) / Double.parseDouble(group.substring(d + 1));
            System.out.println(group + "=" + result);       //DEBUG
            calculation = calculation.substring(0, mOd.start()) + result + calculation.substring(mOd.end());
        }
        temp = Pattern.compile("[*/]").matcher(calculation);
        if (temp.find()) throw new IllegalArgumentException("Invalid operation at " + (temp.start() + 1));

        // Add or subtract
        while (true) {
            Matcher aOs = Pattern.compile(douBle + "[+~]" + douBle).matcher(calculation);
            if (!aOs.find()) break;
            String group = aOs.group();
            int a = group.indexOf("+");
            int s = group.indexOf("~");
            double result;
            if (a != -1) result = Double.parseDouble(group.substring(0, a)) + Double.parseDouble(group.substring(a + 1));
            else result = Double.parseDouble(group.substring(0, s)) - Double.parseDouble(group.substring(s + 1));
            System.out.println(group + "=" + result);       //DEBUG
            calculation = calculation.substring(0, aOs.start()) + result + calculation.substring(aOs.end());
        }
        temp = Pattern.compile("[+~]").matcher(calculation);
        if (temp.find()) throw new IllegalArgumentException("Invalid operation at " + (temp.start() + 1));

        if (!calculation.matches(douBle)) throw new IllegalArgumentException("Invalid calculation format");
        System.out.println();       //DEBUG
        return Double.parseDouble(calculation);
    }
}
