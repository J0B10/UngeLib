package de.ungefroren.utils;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 22.10.2017.
 */
public class ChatColorUtils {
    public static List<String> translateAlternateColorcodes(char altColorChar, List<String> stringList) {
        List<String> newList = new ArrayList<>(stringList.size());
        for (String s: stringList) {
            newList.add(ChatColor.translateAlternateColorCodes(altColorChar, s));
        }
        return newList;
    }
}
