package de.ungefroren.utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Created by Jonas on 06.09.2017.
 */
public abstract class SimpleConfigAbstract {

    protected final File file;
    protected final FileConfiguration config;

    public SimpleConfigAbstract(File file) throws InvalidConfigurationException {
        this(file, YamlConfiguration.loadConfiguration(file));
    }

    public SimpleConfigAbstract(File file, FileConfiguration config) throws InvalidConfigurationException {
        this.file = file;
        this.config = config;
        if (config.getKeys(false).size() == 0)
            throw new InvalidSimpleConfigException("Config is invalid or empty!");
    }

    protected String getString(String key) throws Missing {
        String s = config.getString(key);
        if (s != null) return s;
        else throw new Missing(key);
    }

    protected List<String> getStringList(String key) throws Missing {
        List<String> list = config.getStringList(key);
        if (list != null && list.size() != 0) return list;
        else throw new Missing(key);
    }

    protected int getInt(String key) throws Missing, Invalid {
        String s = this.getString(key);
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            throw new Invalid(key);
        }
    }

    protected double getDouble(String key) throws Missing, Invalid {
        String s = this.getString(key);
        try {
            return Double.parseDouble(s);
        } catch (NumberFormatException e) {
            throw new Invalid(key);
        }
    }

    protected long getLong(String key) throws Missing, Invalid {
        String s = this.getString(key);
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException e) {
            throw new Invalid(key);
        }
    }

    protected boolean getBoolean(String key) throws Missing, Invalid {
        String s = this.getString(key);
        if (s.trim().equalsIgnoreCase("true")) return true;
        else if (s.trim().equalsIgnoreCase("false")) return false;
        else throw new Invalid(key);
    }

    protected <T extends Enum<T>> T getEnum(String key, Class<T> enumType) throws Missing, Invalid {
        String s = this.getString(key).toUpperCase().replace(" ", "_");
        try {
            return Enum.valueOf(enumType, s);
        } catch (IllegalArgumentException e) {
            throw new Invalid(key);
        }
    }

    protected Location getLocation(String key) throws Missing, Invalid {
        String s = this.getString(key);
        Location l = LocationHelper.parse(s);
        if (l != null) return l;
        else throw new Invalid(key);
    }

    protected Material getMaterial(String key) throws Missing, Invalid {
        String s = this.getString(key);
        try {
            Material m = Material.getMaterial(Integer.parseInt(s));
            if (m != null) return m;
            else throw new Invalid(key);
        } catch (NumberFormatException e) {
            Material m = Material.getMaterial(s.toUpperCase().replace(" ", "_"));
            if (m != null) return m;
            else throw new Invalid(key);
        }
    }

    protected ItemStack getItem(String key) throws Missing, Invalid {
        if (!config.contains(key)) throw new Missing(key);
        Material m = this.getMaterial(key + ".material");
        short damage;
        int amount;
        try {
            damage = (short) this.getInt(key + ".damage");
        } catch (Missing e) {
            damage = 0;
        }
        try {
            amount = this.getInt(key + ".amount");
        } catch (Missing e) {
            amount = 1;
        }
        ItemStack stack = new ItemStack(m, amount, damage);
        ItemMeta meta = stack.getItemMeta();
        try {
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', this.getString(key + ".name")));
        } catch (Missing e) {
        }
        try {
            meta.setLore(ChatColorUtils.translateAlternateColorcodes('&', this.getStringList(key + ".lore")));
        } catch (Missing e) {
        } finally {
            stack.setItemMeta(meta);
        }
        return stack;
    }

    protected abstract class DefaultOption<T> {

        private T value;

        public DefaultOption(T defaultValue) throws Invalid {
            try {
                value = of();
            } catch (Missing missing) {
                value = defaultValue;
            }
        }

        protected abstract T of() throws Missing, Invalid;

        public final T get() {
            return value;
        }
    }

    protected abstract class OptionalOption<T> {

        private Optional<T> optional;

        public OptionalOption() throws Invalid {
            try {
                optional = Optional.of(of());
            } catch (Missing missing) {
                optional = Optional.empty();
            }
        }

        protected abstract T of() throws Missing, Invalid;

        public final Optional<T> get() {
            return optional;
        }
    }

    public class InvalidSimpleConfigException extends InvalidConfigurationException {

        public InvalidSimpleConfigException(String message) {
            super("Could not load " + file.getName() + ":\n" + message);
        }
    }

    public class Missing extends InvalidSimpleConfigException {

        public Missing(String missingOption) {
            super("Config option " + missingOption + " is missing!");
        }
    }

    public class Invalid extends InvalidSimpleConfigException {

        public Invalid(String invalidOption) {
            super("Config option " + invalidOption + " is invalid!");
        }
    }
}
