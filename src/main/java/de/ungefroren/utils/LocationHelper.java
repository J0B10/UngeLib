package de.ungefroren.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Created by Jonas on 06.09.2017.
 */
public class LocationHelper {

    public static Location parse(String s) {
        try {
            String[] args = s.split(";");
            double X = Double.parseDouble(args[0]);
            double Y = Double.parseDouble(args[1]);
            double Z = Double.parseDouble(args[2]);
            World world = Bukkit.getWorld(args[3]);
            return new Location(world, X, Y, Z);
        }catch (ArrayIndexOutOfBoundsException | NumberFormatException | NullPointerException e) {
            return null;
        }
    }

    public static String toString(Location l) {
        return l.getX() + ";" + l.getY() + ";" + l.getZ() + ";" + l.getWorld().getName();
    }
}
