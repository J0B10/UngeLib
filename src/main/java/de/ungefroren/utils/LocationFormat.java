package de.ungefroren.utils;

import org.bukkit.Location;

/**
 * Created by Jonas on 16.12.2017.
 */
public enum LocationFormat {

    Minecraft("{x} {y} {z}"),
    BetonQuest("{x};{y};{z};{w}"),
    WorldEdit("{x},{y},{z}"),
    MythicMobs("{w},{x},{y},{z}");


    private final String format;

    LocationFormat(String format) {
        this.format = format;
    }

    public String format(Location location) {
        return this.format
                .replace("{x}", String.valueOf(location.getX()))
                .replace("{y}", String.valueOf(location.getY()))
                .replace("{z}", String.valueOf(location.getZ()))
                .replace("{w}", location.getWorld().getName());
    }

    public LocationFormat nextFormat() {
        switch (this) {
            case Minecraft:
                return BetonQuest;
            case BetonQuest:
                return WorldEdit;
            case WorldEdit:
                return MythicMobs;
            case MythicMobs:
                return Minecraft;
            default:
                return Minecraft;
        }
    }
}
