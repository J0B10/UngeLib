package de.ungefroren.utils.plugin;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

import de.ungefroren.utils.config.UngeLibConfig;
import de.ungefroren.utils.modules.Module;
import de.ungefroren.utils.modules.buildUtils.BuildUtilsModule;
import de.ungefroren.utils.modules.pluginmanager.PluginManagerModule;
import de.ungefroren.utils.modules.warpCompleter.WarpCompleterModule;
import de.ungefroren.utils.modules.wgRegionCompleter.WGRegionCompleterModule;

/**
 * Created by Jonas on 07.09.2017.
 */
public class UngeLibPlugin extends JavaPlugin {

    public static final double version = 1.2;
    private static UngeLibPlugin instance;
    private UngeLibConfig config;
    private Set<Module> modules = new HashSet<>();

    public static UngeLibConfig getUngeLibConfig() {
        return instance.config;
    }

    public static UngeLibPlugin getInstance() {
        return instance;
    }

    public static Logger log() {
        return instance.getLogger();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;
        saveDefaultConfig();
        modules.add(new WarpCompleterModule());
        modules.add(new BuildUtilsModule());
        modules.add(new WarpCompleterModule());
        modules.add(new PluginManagerModule());
        try {
            this.config = new UngeLibConfig(new File(getDataFolder(), "config.yml"));
            String info = "\nModules:\n";
            for (Module m : modules) {
                m.enable();
                info += m.moduleName + ": " + (m.isEnabled() ? "enabled" : "disabled") + "\n";
            }
            getLogger().log(Level.INFO, info);
        } catch (InvalidConfigurationException e) {
            getLogger().log(Level.WARNING, e.getMessage());
        }
    }
}
