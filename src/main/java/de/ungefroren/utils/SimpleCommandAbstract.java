package de.ungefroren.utils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Optional;

/**
 * Created by Jonas on 06.09.2017.
 */
public abstract class SimpleCommandAbstract implements SimpleTabCompleter, TabExecutor {

    public final String usage;
    public final Optional<Permission> requiredPermission;
    public final int minimalArgs;

    public SimpleCommandAbstract(String usage, int minimalArgs) {
        this.usage = usage;
        this.minimalArgs = minimalArgs;
        this.requiredPermission = Optional.empty();
    }

    public SimpleCommandAbstract(Permission reqPermission, String usage, int minimalArgs) {
        this.usage = usage;
        this.requiredPermission = Optional.of(reqPermission);
        this.minimalArgs = minimalArgs;
    }


    @Override
    public abstract List<String> simpleTabComplete(CommandSender sender, Command command, String alias, String[] args);

    public abstract boolean simpleCommand(CommandSender sender, Command command, String alias, String[] args);

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < minimalArgs) {
            sender.sendMessage("§cVerwendung:\n§7" + usage);
            return false;
        }
        if (requiredPermission.isPresent()) {
            if (!sender.hasPermission(requiredPermission.get())) {
                sender.sendMessage("§7[§c§l!§7] Fehlende Berechtigung!");
                return false;
            }
        }
        return simpleCommand(sender, command, label, args);
    }

    public void register(JavaPlugin plugin, String commandName) {
        plugin.getCommand(commandName).setTabCompleter(this);
        plugin.getCommand(commandName).setExecutor(this);
    }
}
