/**
 * Created on 16.04.2018.
 *
 * @author Jonas Blocher
 */
package de.ungefroren.utils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import de.ungefroren.utils.plugin.UngeLibPlugin;

/**
 * Created on 16.04.2018.
 *
 * @author Jonas Blocher
 */
public class ConfirmRequired {

    private final CommandSender commandSender;
    private final String cancelMessage;

    private final BukkitRunnable onConfirm;
    private final int cancelerTaskID;
    private final Listener confirmListener;
    private final Plugin plugin;

    public ConfirmRequired(CommandSender commandSender, BukkitRunnable onConfirm, Plugin plugin) {
        this(commandSender, "§eTippe §7Y§e ein zum Bestätigen!", "§c§oAbgebrochen...", 60,  onConfirm, plugin);
    }

    public ConfirmRequired(CommandSender commandSender, String message, String cancelMessage, int maxTime, BukkitRunnable onConfirm, Plugin plugin) {
        this.commandSender = commandSender;
        this.cancelMessage = cancelMessage;
        this.onConfirm = onConfirm;
        this.plugin = plugin;
        commandSender.sendMessage(message);
        this.confirmListener = (commandSender instanceof Player) ? new PlayerConfirmListener() : new ConsoleConfirmListener();
        this.cancelerTaskID = Bukkit.getScheduler().runTaskLater(plugin, () -> {
            commandSender.sendMessage(cancelMessage);
            HandlerList.unregisterAll(confirmListener);
        }, maxTime * 20L).getTaskId();
    }

    public void cancel() {
        Bukkit.getScheduler().cancelTask(cancelerTaskID);
        HandlerList.unregisterAll(confirmListener);
        commandSender.sendMessage(cancelMessage);
    }

    private class PlayerConfirmListener implements Listener {

        public PlayerConfirmListener() {
            Bukkit.getPluginManager().registerEvents(this, plugin);
        }

        @EventHandler(priority = EventPriority.LOWEST)
        public void onChat(AsyncPlayerChatEvent event) {
            if (!event.getPlayer().getUniqueId().equals(((Player) commandSender).getUniqueId())) return;
            event.setCancelled(true);
            if (event.getMessage().trim().equalsIgnoreCase("y")) {
                Bukkit.getScheduler().cancelTask(cancelerTaskID);
                HandlerList.unregisterAll(confirmListener);
                onConfirm.runTask(plugin);
            } else {
                cancel();
            }
        }

        @EventHandler
        public void onQuit(PlayerQuitEvent event) {
            if (!event.getPlayer().getUniqueId().equals(((Player) commandSender).getUniqueId())) return;
            cancel();
        }
    }

    private class ConsoleConfirmListener implements Listener {

        public ConsoleConfirmListener() {
            Bukkit.getPluginManager().registerEvents(this, plugin);
        }

        @EventHandler(priority = EventPriority.LOWEST)
        public void onCommand(ServerCommandEvent event) {
            event.setCancelled(true);
            if (event.getCommand().trim().equalsIgnoreCase("y")) {
                Bukkit.getScheduler().cancelTask(cancelerTaskID);
                HandlerList.unregisterAll(confirmListener);
                onConfirm.runTask(plugin);
            } else {
                cancel();
            }
        }
    }
}
