package de.ungefroren.utils.modules;

import de.ungefroren.utils.plugin.UngeLibPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 10.10.2017.
 */
public abstract class PluginDependentModule extends Module implements Listener {

    protected JavaPlugin plugin = null;
    protected final String pluginName;

    protected PluginDependentModule(String pluginName) {
        this.pluginName = pluginName;
    }

    @Override
    protected void onEnable() {
        Plugin p = Bukkit.getPluginManager().getPlugin(pluginName);
        if (p != null && p.isEnabled()) {
            onPluginEnable((JavaPlugin) p);
        } else {
            Bukkit.getPluginManager().registerEvents(this, Bukkit.getPluginManager().getPlugin("UngeLib"));
        }
    }

    @Override
    protected void onDisable() {
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public final void listenPluginEnable(PluginEnableEvent event) {
        if (!event.getPlugin().getName().equals(pluginName)) return;
        onPluginEnable((JavaPlugin) event.getPlugin());
    }

    protected void onPluginEnable(JavaPlugin javaPlugin) {
        this.plugin = javaPlugin;
        HandlerList.unregisterAll(this);
        System.out.println("[UngeLib] " + moduleName + " hooked into " + pluginName);
    }

}
