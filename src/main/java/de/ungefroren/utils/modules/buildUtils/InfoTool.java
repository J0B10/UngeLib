package de.ungefroren.utils.modules.buildUtils;

import de.ungefroren.utils.LocationFormat;
import de.ungefroren.utils.plugin.UngeLibPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jonas on 15.12.2017.
 */
public class InfoTool implements Listener {

    public static final ItemStack INFOTOOL = new ItemStack(Material.STICK, 1) {{
        ItemMeta meta = getItemMeta();
        meta.setDisplayName("§bInfo Tool");
        meta.setLore(Arrays.asList("§c§r§0§0§2§5"));
        setItemMeta(meta);
    }};
    private HashMap<UUID, LocationFormat> formats;

    public InfoTool() {
        this.formats = new HashMap<>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            formats.put(player.getUniqueId(), LocationFormat.Minecraft);
        }
        Bukkit.getPluginManager().registerEvents(this, UngeLibPlugin.getInstance());
    }

    @EventHandler
    public void onClickBlock(PlayerInteractEvent event) {
        if (event.getItem() == null) return;
        if (!checkItem(event.getItem())) return;
        if (!event.getPlayer().hasPermission("ungelib.module.buildutils.info")) return;
        Block block;
        Player player = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) block = event.getClickedBlock();
        else if (event.getAction() == Action.RIGHT_CLICK_AIR) block = player.getLocation().getBlock();
        else return;
        if (player.isSneaking()) block = player.getLocation().getBlock();
        String location = getFormat(player).format(block.getLocation());
        ComponentBuilder builder = new ComponentBuilder("");
        builder.append("@[" + location + "]:").color(ChatColor.GRAY)
                .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Kopiere Position")))
                .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, location))
                .append(" ", ComponentBuilder.FormatRetention.FORMATTING)
                .append(block.getType().name().toLowerCase()).color(ChatColor.AQUA)
                .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Kopiere Material")))
                .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, block.getType().name()))
                .append(" ", ComponentBuilder.FormatRetention.FORMATTING)
                .append("(" + block.getTypeId() + ":" + block.getData() + ")").color(ChatColor.BLUE)
                .append("\n", ComponentBuilder.FormatRetention.FORMATTING)
                .append("light: ").color(ChatColor.GRAY)
                .append("[" + block.getLightFromSky() + "S/" + block.getLightFromBlocks() + "B]").color(ChatColor.YELLOW)
                .append(" ", ComponentBuilder.FormatRetention.FORMATTING)
                .append("biome: ").color(ChatColor.GRAY)
                .append(block.getBiome().name().toLowerCase()).color(ChatColor.GREEN)
                .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Kopiere Biom")))
                .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, block.getBiome().name()))
                .append("\n", ComponentBuilder.FormatRetention.FORMATTING)
                .append("state: ").color(ChatColor.GRAY)
                .append(block.getState().getData().toString().toLowerCase()).color(ChatColor.DARK_GRAY);
        if (Bukkit.getPluginManager().isPluginEnabled("WorldGuard")) {
            com.sk89q.worldguard.bukkit.WorldGuardPlugin wg = com.sk89q.worldguard.bukkit.WorldGuardPlugin.inst();
            builder.append("\nregions: ").color(ChatColor.GRAY);
            builder
                    .append("__global__").color(ChatColor.BLUE)
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Kopiere Region")))
                    .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "__global__"))
                    .append("(p=0)").color(ChatColor.DARK_GRAY);
            for (com.sk89q.worldguard.protection.regions.ProtectedRegion region
                    : wg.getRegionManager(block.getWorld()).getApplicableRegions(block.getLocation())) {
                builder
                        .append(", ", ComponentBuilder.FormatRetention.NONE).color(ChatColor.GRAY)
                        .append(region.getId()).color(ChatColor.BLUE)
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Kopiere Region")))
                        .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, region.getId()))
                        .append("(p=" + region.getPriority() + ")").color(ChatColor.DARK_GRAY);
            }

        }
        player.spigot().sendMessage(builder.create());
        event.setCancelled(true);
    }

    @EventHandler
    public void changeLocationFormat(PlayerInteractEvent event) {
        if (event.getItem() == null) return;
        if (!checkItem(event.getItem())) return;
        if (!event.getPlayer().hasPermission("ungelib.module.buildutils.info")) return;
        if (event.getAction() != Action.LEFT_CLICK_AIR) return;
        if (!event.getPlayer().isSneaking()) return;
        LocationFormat newFormat = formats.get(event.getPlayer().getUniqueId()).nextFormat();
        setFormat(event.getPlayer(), getFormat(event.getPlayer()).nextFormat());
        event.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
                                               TextComponent.fromLegacyText("§9Location format changed to §7" + newFormat.name()));
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        formats.put(event.getPlayer().getUniqueId(), LocationFormat.Minecraft);
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        formats.remove(event.getPlayer().getUniqueId());
    }

    //@EventHandler
    public void onClickEntity(PlayerInteractEntityEvent event) {
        //TODO Implement later
    }

    //@EventHandler
    public void onDragNDrop(InventoryInteractEvent event) {
        //TODO Implement later
    }

    private LocationFormat getFormat(Player player) {
        return formats.get(player.getUniqueId());
    }

    private void setFormat(Player player, LocationFormat format) {
        formats.put(player.getUniqueId(), format);
    }

    private boolean checkItem(ItemStack item) {
        if (item.getType() != INFOTOOL.getType()) return false;
        if (item.getDurability() != INFOTOOL.getDurability()) return false;
        if (!item.hasItemMeta()) return false;
        ItemMeta meta = item.getItemMeta();
        ItemMeta INFOTOOL_meta = INFOTOOL.getItemMeta();
        if (!meta.hasDisplayName()) return false;
        if (!meta.hasLore()) return false;
        if (!meta.getDisplayName().equals(INFOTOOL_meta.getDisplayName())) return false;
        List<String> lore = meta.getLore();
        List<String> INFOTOOL_lore = INFOTOOL_meta.getLore();
        if (lore.size() != INFOTOOL_lore.size()) return false;
        for (int i = 0; i < INFOTOOL_meta.getLore().size(); i++) {
            if (!lore.get(i).equals(INFOTOOL_lore.get(i))) return false;
        }
        return true;
    }
}
