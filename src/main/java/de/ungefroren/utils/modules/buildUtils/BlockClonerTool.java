package de.ungefroren.utils.modules.buildUtils;

import de.ungefroren.utils.plugin.UngeLibPlugin;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/**
 * Created by Jonas on 15.12.2017.
 */
public class BlockClonerTool implements Listener {

    public static final ItemStack BLOCKCLONER = new ItemStack(Material.IRON_HOE, 1) {{
        ItemMeta meta = getItemMeta();
        meta.setDisplayName("§5BlockCloner §7(empty)");
        meta.setLore(Arrays.asList("§c§r§0§0§f§f"));
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        setItemMeta(meta);
    }};
    private HashMap<UUID, BlockData> storedBlocks;

    public BlockClonerTool() {
        storedBlocks = new HashMap<>();
        Bukkit.getPluginManager().registerEvents(this, UngeLibPlugin.getInstance());
    }

    @EventHandler
    protected void onClick(PlayerInteractEvent event) {
        if (event.getItem() == null) return;
        if (event.getHand() == EquipmentSlot.OFF_HAND) return;
        if (!event.getPlayer().hasPermission("ungelib.module.buildutils.blockcloner")) return;
        if (!checkItem(event.getItem())) return;
        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (!event.getPlayer().isSneaking()) return;
            Block block = event.getClickedBlock();
            event.setCancelled(true);
            storedBlocks.put(event.getPlayer().getUniqueId(), new BlockData(event.getClickedBlock().getState()));
            ItemStack item = BLOCKCLONER.clone();
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§5BlockCloner §7(" + block.getType().name().toLowerCase().replace("_", " ")
                                        + " : " + block.getData() + ")");
            item.setItemMeta(meta);
            event.getPlayer().getInventory().setItemInMainHand(item);
            return;
        } else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            event.setCancelled(true);
            BlockData data = storedBlocks.get(event.getPlayer().getUniqueId());
            if (data == null) {
                event.getPlayer().getInventory().setItemInMainHand(BLOCKCLONER);
                event.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
                                                       TextComponent.fromLegacyText("§cWähle einen Block aus!"));
                return;
            }
            Block block = event.getClickedBlock().getRelative(event.getBlockFace());
            if (block.getType() != Material.AIR) return;
            block.setType(data.material, true);
            block.setData(data.data);
            event.getPlayer().playSound(block.getLocation(), Sound.BLOCK_STONE_PLACE, 1, 1);
        }
    }

    private boolean checkItem(ItemStack item) {
        if (item.getType() != BLOCKCLONER.getType()) return false;
        if (item.getDurability() != BLOCKCLONER.getDurability()) return false;
        if (!item.hasItemMeta()) return false;
        ItemMeta meta = item.getItemMeta();
        ItemMeta BLOCKCLONER_meta = BLOCKCLONER.getItemMeta();
        if (!meta.hasDisplayName()) return false;
        if (!meta.hasLore()) return false;
        if (!meta.getDisplayName().startsWith("§5BlockCloner ")) return false;
        List<String> lore = meta.getLore();
        List<String> BLOCKCLONER_lore = BLOCKCLONER_meta.getLore();
        if (lore.size() != BLOCKCLONER_lore.size()) return false;
        for (int i = 0; i < BLOCKCLONER_meta.getLore().size(); i++) {
            if (!lore.get(i).equals(BLOCKCLONER_lore.get(i))) return false;
        }
        return true;
    }

    private static class BlockData {

        public final Material material;
        public final byte data;

        public BlockData(Material material, byte data) {
            this.material = material;
            this.data = data;
        }

        public BlockData(BlockState state) {
            this.material = state.getType();
            this.data = state.getRawData();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BlockData data1 = (BlockData) o;
            return data == data1.data &&
                    material == data1.material;
        }

        @Override
        public int hashCode() {
            return Objects.hash(material, data);
        }
    }
}
