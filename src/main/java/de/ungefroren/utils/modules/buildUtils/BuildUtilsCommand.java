package de.ungefroren.utils.modules.buildUtils;

import de.ungefroren.utils.SimpleCommandAbstract;
import de.ungefroren.utils.plugin.UngeLibPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.permissions.Permission;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jonas on 15.12.2017.
 */
public class BuildUtilsCommand extends SimpleCommandAbstract {

    public BuildUtilsCommand() {
        super(new Permission("ungelib.module.buildutils.menu"), "//utils [tool]", 0);
        UngeLibPlugin.getInstance().getCommand("/utils").setExecutor(this);
        UngeLibPlugin.getInstance().getCommand("/utils").setTabCompleter(this);
    }

    @Override
    public List<String> simpleTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) return Arrays.asList("info", "clone");
        return new ArrayList<>();
    }

    @Override
    public boolean simpleCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§7[§c!§7] Command can only be run by players");
            return false;
        }
        Player player = (Player) sender;
        if (args.length == 0) {
            Inventory inventory = Bukkit.createInventory(null, InventoryType.HOPPER, "§6Build Utils");
            if (sender.hasPermission("ungelib.module.buildutils.info")) {
                inventory.addItem(InfoTool.INFOTOOL);
            }
            if (sender.hasPermission("ungelib.module.buildutils.blockcloner")) {
                inventory.addItem(BlockClonerTool.BLOCKCLONER);
            }
            player.openInventory(inventory);
        }
        if (args.length >= 1) {
            switch (args[0].toLowerCase()) {
                case "info":
                case "i":
                    if (!sender.hasPermission("ungelib.module.buildutils.info")) {
                        sender.sendMessage("§7[§c§l!§7] Fehlende Berechtigung!");
                        return false;
                    }
                    player.getInventory().setItemInMainHand(InfoTool.INFOTOOL);
                    return true;
                case "clone":
                case "c":
                    if (!sender.hasPermission("ungelib.module.buildutils.blockcloner")) {
                        sender.sendMessage("§7[§c§l!§7] Fehlende Berechtigung!");
                        return false;
                    }
                    player.getInventory().setItemInMainHand(BlockClonerTool.BLOCKCLONER);
                    return true;
                default:
                    player.sendMessage("§7Verwendung: " + usage);
                    return false;
            }
        }
        return true;
    }
}
