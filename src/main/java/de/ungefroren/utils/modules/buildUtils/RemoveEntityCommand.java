package de.ungefroren.utils.modules.buildUtils;

import de.ungefroren.utils.LocationFormat;
import de.ungefroren.utils.SimpleCommandAbstract;
import de.ungefroren.utils.plugin.UngeLibPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jonas on 17.12.2017.
 */
public class RemoveEntityCommand extends SimpleCommandAbstract {

    public RemoveEntityCommand() {
        super("/removeentity <uuid>", 1);
        UngeLibPlugin.getInstance().getCommand("removeentity").setExecutor(this);
        UngeLibPlugin.getInstance().getCommand("removeentity").setTabCompleter(this);
    }

    @Override
    public List<String> simpleTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return new ArrayList<>();
    }

    @Override
    public boolean simpleCommand(CommandSender sender, Command command, String alias, String[] args) {
        try {
            Entity entity = Bukkit.getEntity(UUID.fromString(args[0]));
            if (entity == null) {
                sender.sendMessage("§7[§c§l!§7] Entity mit dieser UUID existiert nicht");
                return false;
            }
            if (!(sender instanceof Player) || (args.length > 1 && args[1].equalsIgnoreCase("-confirm"))) {
                entity.remove();
                sender.sendMessage("§7[§e§li§7]§e Entity wurde entfernt");
                return true;
            } else if (sender instanceof Player) {
                ComponentBuilder builder = new ComponentBuilder("");
                builder.append(TextComponent.fromLegacyText("§7[§e§li§7]§i Willst du §7" + args[0] + "§e wirklich entfernen?"))
                        .append("\n", ComponentBuilder.FormatRetention.NONE)
                        .append("     >> ").color(ChatColor.GRAY)
                        .append("[Bestätigen]").color(ChatColor.GREEN)
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Klicke um zu bestätigen!")))
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/removeentity " + args[0] + " -confirm"))
                        .append(" <<", ComponentBuilder.FormatRetention.NONE).color(ChatColor.GRAY)
                        .append("\n", ComponentBuilder.FormatRetention.NONE)
                        .append("EntityDaten:").color(ChatColor.DARK_GRAY).bold(true)
                        .append("\n", ComponentBuilder.FormatRetention.NONE)
                        .append("  type=").color(ChatColor.GRAY)
                        .append(entity.getType().name().toLowerCase()).color(ChatColor.WHITE)
                        .append("\n", ComponentBuilder.FormatRetention.NONE)
                        .append("  name=").color(ChatColor.GRAY)
                        .append(entity.getCustomName()).color(ChatColor.WHITE)
                        .append("\n", ComponentBuilder.FormatRetention.NONE)
                        .append("  loc=").color(ChatColor.GRAY)
                        .append(LocationFormat.BetonQuest.format(entity.getLocation())).color(ChatColor.WHITE);
                sender.spigot().sendMessage(builder.create());
                return true;
            } else {
                sender.sendMessage("§7[§e§li§7]§e Confirm with §7/removeentity " + args[0] + " -confirm");
                return true;
            }
        } catch (IllegalArgumentException e) {
            sender.sendMessage("§7[§c§l!§7] Fehlerhafte UUID");
            return false;
        }
    }
}
