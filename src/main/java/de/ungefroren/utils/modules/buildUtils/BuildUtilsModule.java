package de.ungefroren.utils.modules.buildUtils;

import de.ungefroren.utils.modules.Module;
import de.ungefroren.utils.plugin.UngeLibPlugin;
import org.bukkit.event.HandlerList;

/**
 * Created by Jonas on 15.12.2017.
 */
public class BuildUtilsModule extends Module {


    private BlockClonerTool blockClonerTool;
    private InfoTool infoTool;

    @Override
    protected boolean checkEnable() {
        return UngeLibPlugin.getUngeLibConfig().buildUtilsEnabled;
    }

    @Override
    protected void onEnable() {
        blockClonerTool = new BlockClonerTool();
        infoTool = new InfoTool();
        new BuildUtilsCommand();
        new RemoveEntityCommand();
    }

    @Override
    protected void onDisable() {
        HandlerList.unregisterAll(blockClonerTool);
        HandlerList.unregisterAll(infoTool);
    }
}
