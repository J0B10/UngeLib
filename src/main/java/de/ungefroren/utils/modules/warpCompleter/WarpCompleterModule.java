package de.ungefroren.utils.modules.warpCompleter;

import de.ungefroren.utils.SimpleTabCompleter;
import de.ungefroren.utils.modules.PluginDependentModule;
import de.ungefroren.utils.plugin.UngeLibPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 08.10.2017.
 */
public class WarpCompleterModule extends PluginDependentModule implements SimpleTabCompleter {

    public WarpCompleterModule() {
        super("Essentials");
    }

    @Override
    protected boolean checkEnable() {
        return UngeLibPlugin.getUngeLibConfig().warpCompleterEnabled;
    }

    @Override
    public List<String> simpleTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        com.earth2me.essentials.Warps warps = ((com.earth2me.essentials.Essentials) super.plugin).getWarps();
        return new ArrayList<>(warps.getList());
    }

    @Override
    protected void onPluginEnable(JavaPlugin javaPlugin) {
        super.onPluginEnable(javaPlugin);
        super.plugin.getCommand("warp").setTabCompleter(this);
    }
}
