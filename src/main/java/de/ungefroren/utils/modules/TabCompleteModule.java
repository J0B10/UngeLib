/**
 * Created on 15.04.2018.
 *
 * @author Jonas Blocher
 */
package de.ungefroren.utils.modules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.plugin.java.JavaPlugin;

import de.ungefroren.utils.SimpleTabCompleter;
import de.ungefroren.utils.plugin.UngeLibPlugin;

/**
 * Created on 15.04.2018.
 *
 * @author Jonas Blocher
 */
public abstract class TabCompleteModule extends PluginDependentModule implements SimpleTabCompleter {

    private String[] commandAliasses;

    protected TabCompleteModule(String pluginName, String... commandAlisases) {
        super(pluginName);
        this.commandAliasses = commandAlisases;
    }

    @Override
    protected void onPluginEnable(JavaPlugin javaPlugin) {
        super.onPluginEnable(javaPlugin);
        Bukkit.getPluginManager().registerEvents(new Listener() {

            @EventHandler
            public void onTabCompleteEvent(TabCompleteEvent event) {
                if (!checkCommand(event)) return;
                List<String> args = new ArrayList<>(Arrays.asList(event.getBuffer().split(" ")));
                final String alias = args.remove(0).substring(1);
                if (event.getBuffer().endsWith(" ")) args.add("");
                List<String> completations = onTabComplete(event.getSender(), null, alias, args.toArray(new String[0]));
                if (completations != null) event.setCompletions(completations);
            }

        }, UngeLibPlugin.getInstance());
    }

    protected boolean checkCommand(TabCompleteEvent event) {
        String msg = event.getBuffer();
        StringBuilder aliasses = new StringBuilder();
        for (int i = 0; i < commandAliasses.length; i++) {
            aliasses.append(commandAliasses[i]);
            if (i + 1 < commandAliasses.length) aliasses.append('|');
        }
        String regex = "/?(" + pluginName.toLowerCase() +":)?(" + aliasses.toString() + ") .*";
        return msg.matches(regex);
    }
}
