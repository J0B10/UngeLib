/**
 * Created on 21.04.2018.
 *
 * @author Jonas Blocher
 */
package de.ungefroren.utils.modules.pluginmanager.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import de.ungefroren.utils.ConfirmRequired;
import de.ungefroren.utils.SimpleCommandAbstract;
import de.ungefroren.utils.modules.pluginmanager.PluginManagerUtils;
import de.ungefroren.utils.plugin.UngeLibPlugin;

/**
 * Created on 21.04.2018.
 *
 * @author Jonas Blocher
 */
public class ReloadCommand extends SimpleCommandAbstract {

    public ReloadCommand() {
        super("/reload <PluginName> [-f]", 1);
        register(UngeLibPlugin.getInstance(), "reload");
    }

    @Override
    public List<String> simpleTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length > 2) return new ArrayList<>();
        else if (args.length == 2) return Arrays.asList("-f");
        List<String> s = new ArrayList<>();
        Arrays
                .stream(Bukkit.getPluginManager().getPlugins())
                .filter((plugin -> plugin.getName().toLowerCase().startsWith(args[0].toLowerCase())))
                .forEach((plugin -> s.add(plugin.getName())));
        return s;
    }

    @Override
    public boolean simpleCommand(CommandSender sender, Command command, String alias, String[] args) {
        Plugin plugin = PluginManagerUtils.findPlugin(args[0]);
        if (plugin == null) {
            error(sender, args[0], "Could not find Plugin!");
            return false;
        }
        boolean force = false;
        if (args.length != 1) for (String arg : args) {
            if (arg.equalsIgnoreCase("-f")) {
                force = true;
                break;
            }
        }
        String pluginName = plugin.getName();
        try {
            PluginManagerUtils.DependencyCheckResult checkResult = PluginManagerUtils.checkDepends(pluginName);
            if (checkResult.hasDependencys() && !force) {
                final String msg = "§eYou must also reload the following dependencies: \n§7" + checkResult.uniqueDependencyString()
                        + "\n§eType §7§lY§e in chat to confirm!";
                final String ccl = "§c§oCanceled...";
                new ConfirmRequired(sender, msg, ccl, 60, new BukkitRunnable() {
                    @Override
                    public void run() {
                        reload(sender, checkResult);
                    }
                }, UngeLibPlugin.getInstance());
                return false;
            } else {
                return reload(sender, checkResult);
            }
        } catch (PluginManagerUtils.DependencyLoopException e) {
            error(sender, pluginName, e.getMessage());
            info(Bukkit.getConsoleSender(), "§cUnique dependencies: §7" + e.uniqueDependencyString());
            return false;
        }
    }

    private boolean reload(CommandSender sender, PluginManagerUtils.DependencyCheckResult checkResult) {
        try {
            PluginManagerUtils.unload(checkResult);
            PluginManagerUtils.load(checkResult);
            if (checkResult.getAmount() == 0) info(sender, "§aSuccessfully reloaded §2" + checkResult.getPlugin() + "§a!");
            else info(sender, "§aSuccessfully reloaded §2" + checkResult.getPlugin()
                    + "§a and §2" + checkResult.getAmount() + "§a dependencies!");
            return true;
        } catch (Throwable e) {
            error(sender, checkResult.getPlugin(), checkResult.getAmount(), e);
            return false;
        }
    }


    private void error(CommandSender sender, String pluginName, String message) {
        info(sender, "§4Could not reload §7" + pluginName + "§4: §c" + message);
    }

    private void error(CommandSender sender, String pluginName, int dependencies, String message) {
        info(sender, "§4Could not reload §7" + pluginName + "§4 and §7" + dependencies + "§4 dependencies: §c" + message);
    }

    private void info(CommandSender sender, String message) {
        sender.sendMessage(message);
        if (!(sender instanceof ConsoleCommandSender)) {
            Bukkit.getConsoleSender().sendMessage(message);
        }
    }

    private void error(CommandSender sender, String pluginName, Throwable t) {
        error(sender, pluginName, t.getMessage());
        t.printStackTrace();
    }

    private void error(CommandSender sender, String pluginName, int dependencies, Throwable t) {
        error(sender, pluginName, dependencies, t.getMessage());
        t.printStackTrace();
    }
}
