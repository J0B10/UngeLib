/**
 * Created on 21.04.2018.
 *
 * @author Jonas Blocher
 */
package de.ungefroren.utils.modules.pluginmanager.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.Plugin;

import de.ungefroren.utils.SimpleCommandAbstract;
import de.ungefroren.utils.modules.pluginmanager.PluginManagerUtils;
import de.ungefroren.utils.plugin.UngeLibPlugin;

/**
 * Created on 21.04.2018.
 *
 * @author Jonas Blocher
 */
public class UnloadCommand extends SimpleCommandAbstract {

    public UnloadCommand() {
        super("/unload <PluginName>", 1);
        register(UngeLibPlugin.getInstance(), "unload");
    }

    @Override
    public List<String> simpleTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length != 1) return new ArrayList<>();
        List<String> s = new ArrayList<>();
        Arrays
                .stream(Bukkit.getPluginManager().getPlugins())
                .filter((plugin -> plugin.getName().toLowerCase().startsWith(args[0].toLowerCase())))
                .forEach((plugin -> s.add(plugin.getName())));
        return s;
    }

    @Override
    public boolean simpleCommand(CommandSender sender, Command command, String alias, String[] args) {
        Plugin plugin = PluginManagerUtils.findPlugin(args[0]);
        if (plugin == null) {
            error(sender, args[0], "Could not find Plugin!");
            return false;
        }
        String pluginName = plugin.getName();
        try {
            PluginManagerUtils.unloadPlugin(plugin);
            info(sender,"§aSuccessfully unloaded §2" + pluginName + "§a!");
            return true;
        } catch (Throwable e) {
            error(sender, pluginName, e);
            return false;
        }
    }

    private void error(CommandSender sender, String pluginName, String message) {
        info(sender, "§4Could not unload §7" + pluginName + "§4: §c" + message);
    }

    private void info(CommandSender sender, String message) {
        sender.sendMessage(message);
        if (!(sender instanceof ConsoleCommandSender)) {
            Bukkit.getConsoleSender().sendMessage(message);
        }
    }

    private void error(CommandSender sender, String pluginName, Throwable t) {
        error(sender, pluginName, t.getMessage());
        t.printStackTrace();
    }
}
