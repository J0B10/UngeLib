/**
 * Created on 17.04.2018.
 *
 * @author Jonas Blocher
 */
package de.ungefroren.utils.modules.pluginmanager;

import java.io.File;
import java.io.IOException;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginIdentifiableCommand;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;

import de.ungefroren.utils.ReflectionUtils;
import de.ungefroren.utils.plugin.UngeLibPlugin;

/**
 * Created on 17.04.2018.
 *
 * @author Jonas Blocher
 */
public class PluginManagerUtils {


    public static DependencyCheckResult checkDepends(String plugin) throws DependencyLoopException {
        return PluginManagerUtils.checkDepends(plugin, new DependencyCheckResult(plugin), 0);
    }

    private static DependencyCheckResult checkDepends(String pluginName,
                                                      DependencyCheckResult results,
                                                      int deep) throws DependencyLoopException {
        PluginManager pluginManager = Bukkit.getPluginManager();
        if (deep > pluginManager.getPlugins().length) throw new DependencyLoopException(results);
        if (results.dependencyStructure.size() == deep) results.dependencyStructure.add(new HashSet<>());
        Set<String> dependencyStage = results.dependencyStructure.get(deep);
        dependencyStage.add(pluginName);
        results.uniqueDependencies.add(pluginName);
        for (Plugin p : pluginManager.getPlugins()) {
            if (!p.getDescription().getSoftDepend().contains(pluginName)
                    && !p.getDescription().getDepend().contains(pluginName)) continue;
            checkDepends(p.getName(), results, deep + 1);
        }
        return results;
    }

    public static void load(DependencyCheckResult dependencys) throws Throwable {
        for (int i = 0; i < dependencys.dependencyStructure.size(); i++) {
            for (String s : dependencys.dependencyStructure.get(i)) {
                if (isPluginAlreadyLoaded(s)) continue;
                // find plugin file
                File pmpluginfile = findPluginFile(s);
                // ignore if we can't find plugin file
                if (!pmpluginfile.exists()) continue;
                loadPlugin(pmpluginfile);
            }
        }
    }

    public static void unload(DependencyCheckResult dependencys) throws Throwable {
        for (int i = dependencys.dependencyStructure.size() - 1; i >= 0; i--) {
            for (String s : dependencys.dependencyStructure.get(i)) {
                Plugin p  = findPlugin(s);
                if (p == null) continue;
                unloadPlugin(p);
            }
        }
    }

    public static Plugin findPlugin(String pluginName) {
        for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
            if (plugin.getName().equalsIgnoreCase(pluginName)) {
                return plugin;
            }
        }
        return Bukkit.getPluginManager().getPlugin(pluginName);
    }

    public static File findPluginFile(String pluginName) {
        File[] plugins = UngeLibPlugin.getInstance().getDataFolder().getParentFile().listFiles((dir, name) -> name.endsWith(".jar"));
        if (plugins == null || plugins.length == 0) return null;
        for (File pluginfile : plugins) {
            String pName = getPluginName(pluginfile);
            if ((pName != null) && (pluginName.equalsIgnoreCase(pName) || pluginName.equalsIgnoreCase(pName.replace(" ", "_")))) {
                return pluginfile;
            }
        }
        return new File(UngeLibPlugin.getInstance().getDataFolder().getParentFile(), pluginName + ".jar");
    }

    public static String getPluginName(File pluginfile) {
        if (pluginfile.getName().endsWith(".jar")) {
            try (final JarFile jarFile = new JarFile(pluginfile)) {
                JarEntry je = jarFile.getJarEntry("plugin.yml");
                if (je != null) {
                    PluginDescriptionFile plugininfo = new PluginDescriptionFile(jarFile.getInputStream(je));
                    String jarpluginName = plugininfo.getName();
                    jarFile.close();
                    return jarpluginName;
                }
            } catch (IOException | InvalidDescriptionException e) {
            }
        }
        return null;
    }

    public static void loadPlugin(File pluginfile) throws Throwable {
        new ExceptionDetector(() -> {
            PluginManager pluginmanager = Bukkit.getPluginManager();
            // load plugin
            Plugin plugin = pluginmanager.loadPlugin(pluginfile);
            // enable plugin
            plugin.onLoad();
            pluginmanager.enablePlugin(plugin);
        }).run();
    }

    public static boolean isPluginAlreadyLoaded(String pluginname) {
        for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
            if (plugin.getName().equalsIgnoreCase(pluginname)) {
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public static void unloadPlugin(Plugin plugin) throws Throwable {
        new ExceptionDetector(()-> {
            PluginManager pluginmanager = Bukkit.getPluginManager();
            Class<? extends PluginManager> managerclass = pluginmanager.getClass();
            ClassLoader pluginClassLoader = plugin.getClass().getClassLoader();
            // disable plugin
            pluginmanager.disablePlugin(plugin);
            // kill threads if any
            for (Thread thread : Thread.getAllStackTraces().keySet()) {
                if (thread.getClass().getClassLoader() == pluginClassLoader) {
                    thread.interrupt();
                    thread.join(2000);
                    if (thread.isAlive()) {
                        thread.stop();
                    }
                }
            }
            // remove from plugins field
            ((List<Plugin>) ReflectionUtils.getField(pluginmanager, "plugins")).remove(plugin);
            // remove from lookupnames field
            ((Map<String, Plugin>) ReflectionUtils.getField(pluginmanager, "lookupNames")).values().remove(plugin);
            // remove from commands field
            CommandMap commandMap = (CommandMap) ReflectionUtils.getField(managerclass, "commandMap").get(pluginmanager);
            Collection<Command> commands = (Collection<Command>) ReflectionUtils.invokeMethod(commandMap, "getCommands");
            for (Command cmd : new LinkedList<Command>(commands)) {
                if (cmd instanceof PluginIdentifiableCommand) {
                    PluginIdentifiableCommand plugincommand = (PluginIdentifiableCommand) cmd;
                    if (plugincommand.getPlugin().getName().equalsIgnoreCase(plugin.getName())) {
                        removeCommand(commandMap, commands, cmd);
                    }
                } else if (cmd.getClass().getClassLoader() == pluginClassLoader) {
                    removeCommand(commandMap, commands, cmd);
                }
            }
            // close file in url classloader
            if (pluginClassLoader instanceof URLClassLoader) {
                URLClassLoader urlloader = (URLClassLoader) pluginClassLoader;
                urlloader.close();
            }
        }).run();
    }

    private static void removeCommand(CommandMap commandMap,
                                      Collection<Command> commands,
                                      Command cmd) throws NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException {
        cmd.unregister(commandMap);
        if (commands.getClass().getSimpleName().equals("UnmodifiableCollection")) {
            @SuppressWarnings("unchecked")
            Collection<Command> original = (Collection<Command>) ReflectionUtils.getField(commands, "c");
            original.remove(cmd);
        } else {
            commands.remove(cmd);
        }
    }


    public static class DependencyCheckResult {

        private final String plugin;
        private List<Set<String>> dependencyStructure;
        private Set<String> uniqueDependencies;

        public DependencyCheckResult(String plugin) {
            this.plugin = plugin;
            this.dependencyStructure = new ArrayList<>();
            uniqueDependencies = new HashSet<>();
        }

        public String getPlugin() {
            return plugin;
        }

        public Set<String> getUniqueDependencies() {
            uniqueDependencies.remove(plugin);
            return Collections.unmodifiableSet(uniqueDependencies);
        }

        public String uniqueDependencyString() {
            StringJoiner joiner = new StringJoiner(", ");
            this.uniqueDependencies.forEach(joiner::add);
            return joiner.toString();
        }

        public List<Set<String>> getDependencyStructure() {
            return Collections.unmodifiableList(dependencyStructure);
        }

        public boolean hasDependencys() {
            uniqueDependencies.remove(plugin);
            return !uniqueDependencies.isEmpty();
        }

        public int getAmount() {
            uniqueDependencies.remove(plugin);
            return uniqueDependencies.size();
        }
    }

    public static class DependencyLoopException extends Exception {

        private final Set<String> uniqueDependencies;

        public DependencyLoopException(DependencyCheckResult results) {
            super("Circular dependency detected!");
            this.uniqueDependencies = new HashSet<>(results.uniqueDependencies.size() + 1);
            this.uniqueDependencies.add(results.plugin);
            this.uniqueDependencies.addAll(results.uniqueDependencies);
        }

        public Set<String> getUniqueDependencies() {
            return Collections.unmodifiableSet(this.uniqueDependencies);
        }

        public String uniqueDependencyString() {
            StringJoiner joiner = new StringJoiner(", ");
            this.uniqueDependencies.forEach(joiner::add);
            return joiner.toString();
        }

    }
}
