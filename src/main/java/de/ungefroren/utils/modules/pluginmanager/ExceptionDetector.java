/**
 * Created on 06.05.2018.
 *
 * @author Jonas Blocher
 */
package de.ungefroren.utils.modules.pluginmanager;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

import org.bukkit.Bukkit;

/**
 * Created on 06.05.2018.
 *
 * @author Jonas Blocher
 */
public class ExceptionDetector {

    private final Runnable run;
    private Throwable thrown;

    public ExceptionDetector(Runnable run) {
        this.run = run;
        this.thrown = null;
    }

    public void run() throws Throwable {
        final Filter oldFilter = Bukkit.getServer().getLogger().getFilter();
        Bukkit.getServer().getLogger().setFilter(this::isLoggable);
        try {
            this.run.run();
        } catch (Throwable t) {
            if (thrown == null) thrown = t;
        }
        Bukkit.getServer().getLogger().setFilter(oldFilter);
        if (thrown != null) throw thrown;

    }

    private boolean isLoggable(LogRecord record) {
        final Throwable thrown = record.getThrown();
        if (thrown == null) return true;
        if (this.thrown == null) this.thrown = thrown;
        return false;
    }

    public interface Runnable {
        void run() throws Throwable;
    }

}
