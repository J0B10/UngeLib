/**
 * Created on 17.04.2018.
 *
 * @author Jonas Blocher
 */
package de.ungefroren.utils.modules.pluginmanager;

import de.ungefroren.utils.modules.Module;
import de.ungefroren.utils.modules.pluginmanager.commands.LoadCommand;
import de.ungefroren.utils.modules.pluginmanager.commands.ReloadCommand;
import de.ungefroren.utils.modules.pluginmanager.commands.UnloadCommand;
import de.ungefroren.utils.plugin.UngeLibPlugin;

/**
 * Created on 17.04.2018.
 *
 * @author Jonas Blocher
 */
public class PluginManagerModule extends Module {

    @Override
    protected boolean checkEnable() {
        return UngeLibPlugin.getUngeLibConfig().pluginManagerEnabled;
    }

    @Override
    protected void onEnable() {
        new LoadCommand();
        new ReloadCommand();
        new UnloadCommand();
    }

    @Override
    protected void onDisable() {

    }
}
