package de.ungefroren.utils.modules.wgRegionCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import de.ungefroren.utils.ConfirmRequired;
import de.ungefroren.utils.modules.TabCompleteModule;
import de.ungefroren.utils.plugin.UngeLibPlugin;
import net.milkbowl.vault.permission.Permission;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 10.10.2017.
 */
public class WGRegionCompleterModule extends TabCompleteModule {

    public WGRegionCompleterModule() {
        super("WorldGuard", "rg", "region", "regions");
    }

    @Override
    protected boolean checkEnable() {
        return UngeLibPlugin.getUngeLibConfig().WGRegionCompleterEnabled;
    }

    @Override
    public List<String> simpleTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) return Arrays.asList("define",
                                                   "remove",
                                                   "redefine",
                                                   "addmember",
                                                   "addowner",
                                                   "removemember",
                                                   "removeowner",
                                                   "select",
                                                   "info",
                                                   "list",
                                                   "flag",
                                                   "setpriority",
                                                   "setparent");
        switch (args[0]) {
            case "define":
            case "create":
            case "d":
            case "remove":
            case "delete":
            case "rem":
            case "del":
            case "redefine":
            case "update":
            case "move":
            case "select":
            case "sel":
            case "s":
            case "info":
            case "i":
            case "setparent":
            case "teleport":
                return simpleRegionComplete(sender, command, alias, args);
            case "addmember":
            case "addmem":
            case "am":
            case "addowner":
            case "ao":
                return simpleAddMemberComplete(sender, command, alias, args);
            case "removemember":
            case "remmember ":
            case "remmem ":
            case "rm":
            case "removeowner":
            case "ro":
                return simpleDelMemberComplete(sender, command, alias, args);
            case "setpriority":
            case "priority":
            case "pri":
                return simpleSetpriorityComplete(sender, command, alias, args);
            case "flag":
                return simpleFlagComplete(sender, command, alias, args);
        }
        return null;
    }

    private List<String> simpleFlagComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 2) return simpleRegionComplete(sender, command, alias, args);
        if (args.length == 3) return listFlags();
        com.sk89q.worldguard.bukkit.WorldGuardPlugin wg = (WorldGuardPlugin) plugin;
        com.sk89q.worldguard.protection.flags.Flag flag = wg.getFlagRegistry().get(args[2]);
        if (flag == null) return null;
        if (flag instanceof com.sk89q.worldguard.protection.flags.StateFlag) return Arrays.asList("allow", "deny");
        return new ArrayList<>();
    }

    private List<String> simpleSetpriorityComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 2) return simpleRegionComplete(sender, command, alias, args);
        if (!(sender instanceof Player)) return new ArrayList<>();
        if (args.length == 3) return Arrays.asList(String.valueOf(getMaxPriority(((Player) sender).getLocation())));
        return new ArrayList<>();
    }

    private List<String> simpleDelMemberComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 3 && args[1].equals("-a")) return simpleRegionComplete(sender, command, alias, args);
        return simpleAddMemberComplete(sender, command, alias, args);
    }

    private List<String> simpleAddMemberComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 2) return simpleRegionComplete(sender, command, alias, args);
        if (args[args.length - 1] != null && args[args.length - 1].startsWith("g:")) {
            return simpleGroupComplete(sender, command, alias, args);
        }
        return null;
    }

    private List<String> simpleGroupComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (Bukkit.getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Permission> rsp = Bukkit
                    .getServicesManager()
                    .getRegistration(net.milkbowl.vault.permission.Permission.class);
            net.milkbowl.vault.permission.Permission permission = rsp.getProvider();
            List<String> completations = new ArrayList<>();
            for (String s : permission.getGroups()) {
                completations.add("g:" + s);
            }
            return completations;
        }
        return null;
    }

    private List<String> simpleRegionComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (!(sender instanceof Player)) return new ArrayList<>();
        com.sk89q.worldguard.bukkit.WorldGuardPlugin wg = (WorldGuardPlugin) plugin;
        return new ArrayList<>(wg.getRegionManager(((Player) sender).getWorld()).getRegions().keySet());
    }

    private int getMaxPriority(Location l) {
        com.sk89q.worldguard.bukkit.WorldGuardPlugin wg = (WorldGuardPlugin) plugin;
        com.sk89q.worldguard.protection.ApplicableRegionSet regions = wg.getRegionManager(l.getWorld()).getApplicableRegions(l);
        int prio = 10;
        for (com.sk89q.worldguard.protection.regions.ProtectedRegion rg : regions) {
            if (prio <= rg.getPriority()) prio = (rg.getPriority() / 10) * 10 + 10;
        }
        return prio;
    }

    private List<String> listFlags() {
        com.sk89q.worldguard.bukkit.WorldGuardPlugin wg = (WorldGuardPlugin) plugin;
        List<String> list = new ArrayList<>();
        for (com.sk89q.worldguard.protection.flags.Flag flag : wg.getFlagRegistry()) {
            list.add(flag.getName());
        }
        return list;
    }

    private List<String> removeInvalidAlias(String invalid, List<String> aliases) {
        aliases.remove(invalid);
        return aliases;
    }
}
