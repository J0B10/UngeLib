package de.ungefroren.utils.modules;

/**
 * Licensed under GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/ungefroren/SchemGui/blob/master/LICENSE
 * <p>
 * Created by Jonas on 08.10.2017.
 */
public abstract class Module {

    public final String moduleName;

    private boolean enabled = false;

    protected Module() {
        this.moduleName = this.getClass().getName();
    }

    public final boolean enable() {
        if (isEnabled()) return true;
        if (!checkEnable()) return false;
        try {
            onEnable();
            enabled = true;
            return true;
        } catch (Exception e) {
            enabled = false;
            e.printStackTrace();
            return false;
        }
    }

    public final void disable() {
        enabled = false;
        onDisable();
    }

    public final boolean isEnabled() {
        return enabled;
    }

    protected abstract boolean checkEnable();

    protected abstract void onEnable();

    protected abstract void onDisable();

}
